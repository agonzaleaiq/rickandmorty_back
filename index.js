const express = require('express')
const usersRoutes = require('./src/users/users.routes')
const charactersRoutes = require('./src/characters/characters.routes')
const favoritesRoutes = require('./src/favorites/favorites.routes')
const auth = require('./src/middleware/auth')
const cors = require('cors')
const app = express()
const port = 8080
require('dotenv').config()

app.use(cors());

app.use(express.json());

app.use('/user', usersRoutes);
app.use('/characters', charactersRoutes);
app.use('/favorites', auth, favoritesRoutes);

app.listen(port, () => {
    // console.log('require(\'crypto\').randomBytes(64).toString(\'hex\')', require('crypto').randomBytes(64).toString('hex'))
    console.log(`Port listening at http://localhost:${port}`)
})


