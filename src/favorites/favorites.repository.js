const configuration = require('../configuration')

function getFavorites(id, callback) {

    let query = `select id_card from authentication.favs where id_users='${id}'`
    console.log(query)
    configuration.executeQuery(query, callback)

}

function insertFavorite(objVariables, callback) {
    let query = `insert into authentication.favs(id_users, id_card) values ('${objVariables.idUser}', '${objVariables.idCard}')`
    console.log(query)
    configuration.executeQuery(query, callback)
}

function removeFavorite(objVariables, callback) {
    let query = `delete from authentication.favs where id_users='${objVariables.idUser}' and id_card='${objVariables.idCard}'`
    console.log(query)
    configuration.executeQuery(query, callback)
}

module.exports = {
    getFavorites,
    insertFavorite,
    removeFavorite
};