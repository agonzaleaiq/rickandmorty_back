const express = require('express')
const router = express.Router()
const favoriteController = require('./favorites.controller')

router.get('', favoriteController.getFavorites)

router.post('', favoriteController.postFavorites)

router.delete('/:id', favoriteController.deleteFavorites)

module.exports = router
