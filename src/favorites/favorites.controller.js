const favoriteService = require('./favorites.service')

getFavorites = async (req, res) => {
    const payload = {
        idUser: req.query.id_user
    }
    await favoriteService.getFavorites(payload, res)
}

postFavorites = (req, res) => {
    const payload = {
        idUser: req.body.idUser,
        idCard: req.body.idCard
    }
    favoriteService.postFavorites(payload, res)
}


deleteFavorites = (req, res) => {
    const payload = {
        idUser: req.body.idUser,
        idCard: req.body.idCard
    }
    favoriteService.deleteFavorites(payload, res)
}

module.exports = {
    getFavorites,
    postFavorites,
    deleteFavorites
}