const postgresql = require('./favorites.repository')

getFavorites = (payload, res) => {
    postgresql.getFavorites(payload.idUser, (resPG, errorPG) => {
        if (errorPG) {
            res.status(400).json({error: errorPG})
        } else {
            const result = []
            resPG.rows.forEach(f => {
                    result.push(f.id_card)
                }
            )
            res.status(200).json({
                count: result.length,
                result: result,
            })
        }
    })
}

postFavorites = (payload, res) => {
    postgresql.insertFavorite(payload, (resPG, errorPG) => {
        if (errorPG) {
            res.status(400).json({error: errorPG})
        } else {
            res.status(200).json({
                count: resPG.rows.length,
                result: resPG.rows,
            })
        }
    })
}

deleteFavorites = (payload, res) => {
    postgresql.removeFavorite(payload, (resPG, errorPG) => {
        if (errorPG) {
            res.status(400).json({error: errorPG})
        } else {
            res.status(200).json({
                count: resPG.rows.length,
                result: resPG.rows,
            })
        }
    })
}

module.exports = {
    getFavorites,
    postFavorites,
    deleteFavorites
}