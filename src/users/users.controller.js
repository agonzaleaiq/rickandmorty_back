const userService = require('./users.service')
const jwt = require('jsonwebtoken');

register = async (req, res) => {
    const payload = {
        email: req.body.email
    }
    await userService.validateEmail(payload, res, (serviceResponse) => {
        if (serviceResponse) {
            payload.name = req.body.name
            payload.password = req.body.password
            userService.insertUser(payload, res)
        }
    })
}

login = (req, res) => {
    const payload = {
        email: req.body.email,
        password: req.body.password
    }
    userService.user(payload, res, (serviceResponse) => {
        if (serviceResponse) {
            res.status(200).json({
                count: serviceResponse.rows.length,
                result: serviceResponse.rows,
                message: 'User logged correctly',
                token: generateAccessToken(serviceResponse.rows[0])
            })
        }
    })
}

function generateAccessToken(username) {
    return jwt.sign(username, process.env.TOKEN_SECRET, {expiresIn: '1800s'});
}

module.exports = {
    register,
    login,
}