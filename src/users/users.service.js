const postgresql = require('./users.repository')

validateEmail = (payload, res, callback) => {
    postgresql.validateEmail(payload, (resPG, errorPG) => {
            if (errorPG) {
                res.status(400).json({error: errorPG})
            } else if (resPG.rowCount === 0) {
                callback(true, undefined)
            } else {
                res.status(212).json({message: 'This email is already in use, please try with another'})
            }
        }
    )
}

insertUser = (payload, res) => {
    postgresql.insertUser(payload, (resPG, errorPG) => {
        if (errorPG) {
            res.status(400).json({error: errorPG})
        } else {
            res.status(201).json({
                count: resPG.rows.length,
                result: resPG.rows,
                message: 'Logged successfully!'
            })
        }
    })
}

user = (payload, res, callback) => {
    postgresql.loginUser(payload, (resPG, errorPG) => {
            if (errorPG) {
                res.status(400).json({error: errorPG})
            } else if (resPG.rows.length === 0)
                res.status(212).json({
                    message: 'Email and password combination is incorrect'
                })
            else
                callback(resPG, undefined)
        }
    )
}

module.exports = {
    validateEmail,
    insertUser,
    user
}