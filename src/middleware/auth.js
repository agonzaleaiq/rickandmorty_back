const jwt = require('jsonwebtoken');

authenticateToken = (req, res, next) => {
    const token = req.headers['authorization'].split(' ')[1];
    console.log('token', token)
    if (!token) return res.status(401).send({auth: false, message: 'No token provided.'});

    jwt.verify(token, process.env.TOKEN_SECRET, function (err) {
        console.log('err', err)
        if (err) return res.status(401).send({auth: false, message: 'Failed to authenticate token.'});

        next()
    });
}

module.exports = authenticateToken