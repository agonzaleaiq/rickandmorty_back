create database ricky;
create schema authentication;
create table authentication.users
(
    name varchar not null,
    email varchar not null,
    id serial not null
);

create unique index users_email_uindex
    on authentication.users (email);

create unique index users_id_uindex
    on authentication.users (id);

alter table authentication.users
    add constraint users_pk
        primary key (id);
alter table authentication.users
    add password varchar not null;


create table authentication.favs
(
    id_users integer not null
        constraint users_id
            references authentication.users,
    id_card  integer not null
        constraint favs_pk
            primary key
);

alter table authentication.favs
    owner to postgres;