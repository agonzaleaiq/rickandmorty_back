const {Client} = require('pg')
const connection = {
    user: 'postgres',
    host: 'localhost',
    database: 'ricky',
    password: 'postgres',
    port: 5432,
}

executeQuery = (query, callback) => {
    const client = new Client(connection);
    client.connect()
    client.query(query, (err, res) => {
        if (err) {
            callback(undefined, 'errorPG ' + err)
            client
                .end()
                .then(() => console.log('client has disconnected'))
                .catch(err => console.error('error during disconnection', err.stack))
        } else {
            callback(res, undefined);
            client
                .end()
                .then(() => console.log('client has disconnected'))
                .catch(err => console.error('error during disconnection', err.stack))
        }
    })
}


module.exports = {
    executeQuery,
}