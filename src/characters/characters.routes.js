const express = require('express')
const router = express.Router()
const charactersController = require('./characters.controller')

router.get('', charactersController.listCharacters)

router.get('/:id',  charactersController.getCharacter)

module.exports = router
