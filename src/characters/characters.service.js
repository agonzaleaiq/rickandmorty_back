const request = require('request');

requestToRickAndMorty = (requestOptions, res) => {

    request(requestOptions, (err, response, body) => {

        if (err) {
            console.log(err);
        } else if (response.statusCode === 200) {
            res.status(200).json({info: body.info, result: body.results})
        } else {
            console.log('response.statusCode', response.statusCode);
        }
    });
}

module.exports = {
    requestToRickAndMorty
}