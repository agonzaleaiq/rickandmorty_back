const characterService = require('./characters.service');

listCharacters = async (req, res) => {
    const payload = {
        page: req.query.page ? req.query.page : 0,
        name: req.query.name ? req.query.name : '',
        status: req.query.status ? req.query.status : ''
    }
    const requestOptions = {
        url: process.env.BASE_URL + `?page=${payload.page}&name=${payload.name}&status=${payload.status}`,
        method: 'GET',
        json: {},
    };
    characterService.requestToRickAndMorty(requestOptions, res)
}

getCharacter = async (req, res) => {
    const requestOptions = {
        url: process.env.BASE_URL + '/' + req.params.id,
        method: 'GET',
        json: {},
    };
    characterService.requestToRickAndMorty(requestOptions, res)
}

module.exports = {
    listCharacters,
    getCharacter
}